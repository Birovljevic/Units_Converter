package stjepan.units_converter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    @BindView(R.id.ibLength) ImageButton ibLength;
    @BindView(R.id.ibMass) ImageButton ibMass;
    @BindView(R.id.ibSpeed) ImageButton ibSpeed;
    @BindView(R.id.ibTemperature) ImageButton ibTemperature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.ibLength, R.id.ibMass, R.id.ibSpeed, R.id.ibTemperature})
    public void getActivity(View v){
        Intent intent = new Intent();
        if (v.getId() == R.id.ibLength){
            intent.setClass(this, LengthActivity.class);
        }
        else if (v.getId() == R.id.ibMass){
            intent.setClass(this, MassActivity.class);
        }
        else if (v.getId() == R.id.ibSpeed){
            intent.setClass(this, SpeedActivity.class);
        }
        else if (v.getId() == R.id.ibTemperature){
            intent.setClass(this, TemperatureActivity.class);
        }
        startActivity(intent);
    }

    //TO DO: onLongClick toast with descriptions
}
