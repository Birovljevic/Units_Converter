package stjepan.units_converter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public class SpeedActivity extends Activity {

    @BindView(R.id.spSpeedFrom)
    Spinner spSpeedFrom;
    @BindView(R.id.spSpeedTo) Spinner spSpeedTo;
    @BindView(R.id.btnSpeedConvert)
    Button btnSpeedConvert;
    @BindView(R.id.etSpeedValueToConvert)
    EditText etSpeedValueToConvert;

    private String convertFrom = "";
    private String convertTo = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speed);
        ButterKnife.bind(this);
        initializeSpinner();
    }
    private void initializeSpinner(){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.speed_array,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSpeedFrom.setAdapter(adapter);
        spSpeedTo.setAdapter(adapter);
    }
    @OnItemSelected(R.id.spSpeedFrom)
    public void spinnerItemFrom(Spinner spinner, int position){
        this.convertFrom = spinner.getItemAtPosition(position).toString().split(" ")[0];
    }
    @OnItemSelected(R.id.spSpeedTo)
    public void spinnerItemTo(Spinner spinner, int position){
        this.convertTo = spinner.getItemAtPosition(position).toString().split(" ")[0];
    }
    @OnClick(R.id.btnSpeedConvert)
    public void convertSpeed(){
        Double valueToConvert = 0.0;
        try{
            valueToConvert = getValueToConvert(Double.parseDouble(this.etSpeedValueToConvert.getText().toString()));
            showResult(valueToConvert);
        }catch (Exception e){
            Toast.makeText(this, "Bad input!", Toast.LENGTH_SHORT).show();
        }
    }
    private void showResult(Double valueToConvert) {
        Double result = 0.0;
        Intent intent = new Intent(this, ResultActivity.class);

        intent.putExtra(ResultActivity.KEY_CONVERT_FROM, this.convertFrom);
        intent.putExtra(ResultActivity.KEY_CONVERT_TO, this.convertTo);
        intent.putExtra(ResultActivity.KEY_VALUE, valueToConvert);

        if (this.convertFrom.equals(this.convertTo)){
            result = valueToConvert;
        }
        if (this.convertFrom.equals("kilometers/hour") && this.convertTo.equals("miles/hour")){
            result = valueToConvert * 0.62137119223733;
        }
        if (this.convertFrom.equals("miles/hour") && this.convertTo.equals("kilometers/hour")){
            result = valueToConvert / 0.62137119223733;
        }
        intent.putExtra(ResultActivity.KEY_CONVERTED_VALUE, result);
        startActivity(intent);
    }
    private Double getValueToConvert(double valueToConvert) throws InputException{
        if (valueToConvert <= 0.0){
            throw new InputException();
        }
        return valueToConvert;
    }
}
