package stjepan.units_converter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResultActivity extends Activity {

    public static final String KEY_VALUE = "value";
    public static final String KEY_CONVERTED_VALUE = "convertedValue";
    public static final String KEY_CONVERT_FROM = "convertFrom";
    public static final String KEY_CONVERT_TO = "convertTo";

    @BindView(R.id.tvResultFrom) TextView tvResultFrom;
    @BindView(R.id.tvResultValue) TextView tvResultValue;
    @BindView(R.id.tvResultTo) TextView tvResultTo;
    @BindView(R.id.tvResultConvertedValue) TextView tvResultConvertedValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        ButterKnife.bind(this);

        Intent startingIntent = this.getIntent();
        this.handleExtraData(startingIntent);
    }
    //region Handle Extra Data
    private void handleExtraData(Intent startingIntent){
        if (startingIntent.hasExtra(KEY_CONVERT_FROM)){
            tvResultFrom.setText(startingIntent.getStringExtra(KEY_CONVERT_FROM));
        }
        if (startingIntent.hasExtra(KEY_VALUE)){
            Double value = startingIntent.getDoubleExtra(KEY_VALUE, 0);
            tvResultValue.setText(String.valueOf(value));
        }
        if (startingIntent.hasExtra(KEY_CONVERT_TO)){
            tvResultTo.setText(startingIntent.getStringExtra(KEY_CONVERT_TO));
        }
        if (startingIntent.hasExtra(KEY_CONVERTED_VALUE)){
            Double convertedValue = startingIntent.getDoubleExtra(KEY_CONVERTED_VALUE, 0);
            tvResultConvertedValue.setText(String.valueOf(convertedValue));
        }
    }
    //endregion
}
