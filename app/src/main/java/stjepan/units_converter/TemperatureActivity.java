package stjepan.units_converter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public class TemperatureActivity extends Activity {

    @BindView(R.id.etTempValueToConvert) EditText etTempValueToConvert;
    @BindView (R.id.spTempFrom) Spinner spTempFrom;
    @BindView(R.id.spTempTo) Spinner spTempTo;

    private String convertFrom = "", convertTo = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature);
        ButterKnife.bind(this);
        initializeSpinner();

    }
    private void initializeSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.temperature_array,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTempFrom.setAdapter(adapter);
        spTempTo.setAdapter(adapter);
    }

    @OnItemSelected(R.id.spTempFrom)
    public void spinnerItemFrom(Spinner spinner, int position){
        this.convertFrom = spinner.getItemAtPosition(position).toString().split(" ")[0];
    }
    @OnItemSelected(R.id.spTempTo)
    public void spinnerItemTo(Spinner spinner, int position){
        this.convertTo = spinner.getItemAtPosition(position).toString().split(" ")[0];
    }
    @OnClick(R.id.btnTempConvert)
    public void convertTemp(){
        Double valueToConvert = 0.0;
        try{
            valueToConvert = getValueToConvert(Double.parseDouble(this.etTempValueToConvert.getText().toString()));
            showResult(valueToConvert);
        }catch (Exception e){
            Toast.makeText(this, "Bad input", Toast.LENGTH_SHORT).show();
        }
    }
    private void showResult(Double valueToConvert) {
        Double result = 0.0;
        Intent intent = new Intent(this, ResultActivity.class);

        intent.putExtra(ResultActivity.KEY_CONVERT_FROM, this.convertFrom);
        intent.putExtra(ResultActivity.KEY_CONVERT_TO, this.convertTo);
        intent.putExtra(ResultActivity.KEY_VALUE, valueToConvert);
        if (this.convertFrom.equals(this.convertTo)){
            result = valueToConvert;
        }
        if (this.convertFrom.equals("Celsius") && this.convertTo.equals("Kelvin")){
            result = valueToConvert + 273.15;
        }
        if (this.convertFrom.equals("Kelvin") && this.convertTo.equals("Celsius")){
            result = valueToConvert - 273.15;
        }
        intent.putExtra(ResultActivity.KEY_CONVERTED_VALUE, result);
        startActivity(intent);
    }
    private Double getValueToConvert(Double valueToConvert) throws InputException{
        if (valueToConvert <= 0.0){
            throw new InputException();
        }
        return valueToConvert;
    }
}
