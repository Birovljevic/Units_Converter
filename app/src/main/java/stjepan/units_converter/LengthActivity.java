package stjepan.units_converter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public class LengthActivity extends Activity {

    @BindView(R.id.spLengthFrom) Spinner spLengthFrom;
    @BindView(R.id.spLengthTo) Spinner spLengthTo;
    @BindView(R.id.btnLengthConvert) Button btnLengthConvert;
    @BindView(R.id.etLengthValueToConvert) EditText etLengthValueToConvert;

    private String convertFrom = "";
    private String convertTo = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_length);
        ButterKnife.bind(this);
        initializeSpinner();
    }
    private void initializeSpinner(){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.length_array,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spLengthFrom.setAdapter(adapter);
        spLengthTo.setAdapter(adapter);
    }
    @OnItemSelected(R.id.spLengthFrom)
    public void spinnerItemFrom(Spinner spinner, int position){
        this.convertFrom = spinner.getItemAtPosition(position).toString().split(" ")[0];
    }
    @OnItemSelected(R.id.spLengthTo)
    public void spinnerItemTo(Spinner spinner, int position){
        this.convertTo = spinner.getItemAtPosition(position).toString().split(" ")[0];
    }
    @OnClick(R.id.btnLengthConvert)
    public void convertLength(){

        Double valueToConvert = 0.0;
        try {
            valueToConvert = getValueToConvertValid(Double.parseDouble(this.etLengthValueToConvert.getText().toString()));
            showResult(valueToConvert);
        } catch(Exception e){
            Toast.makeText(this, "Bad input", Toast.LENGTH_SHORT).show();
        }
    }
    private void showResult(Double valueToConvert) {
        Double result = 0.0;
        Intent intent = new Intent(this, ResultActivity.class);

        intent.putExtra(ResultActivity.KEY_CONVERT_FROM, this.convertFrom);
        intent.putExtra(ResultActivity.KEY_CONVERT_TO, this.convertTo);
        intent.putExtra(ResultActivity.KEY_VALUE, valueToConvert);

        if (this.convertFrom.equals(this.convertTo)){
            result = valueToConvert;
        }
        if (this.convertFrom.equals("meters") && this.convertTo.equals("miles")){
            result = valueToConvert * 0.00062137;
        }
        if (this.convertFrom.equals("miles") && this.convertTo.equals("meters")){
            result = valueToConvert / 0.00062137;
        }
        intent.putExtra(ResultActivity.KEY_CONVERTED_VALUE, result);
        startActivity(intent);
    }
    private Double getValueToConvertValid(Double valueToConvert) throws InputException {
        if (valueToConvert <= 0.0){
            throw new InputException();
        }
        return valueToConvert;
    }
}
