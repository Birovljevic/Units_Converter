package stjepan.units_converter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public class MassActivity extends Activity {

    @BindView(R.id.spMassFrom)
    Spinner spMassFrom;
    @BindView(R.id.spMassTo) Spinner spMassTo;
    @BindView(R.id.btnMassConvert)
    Button btnMassConvert;
    @BindView(R.id.etMassValueToConvert)
    EditText etMassValueToConvert;

    private String convertFrom = "";
    private String convertTo = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mass);
        ButterKnife.bind(this);
        initializeSpinner();
    }

    private void initializeSpinner(){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.mass_array,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMassFrom.setAdapter(adapter);
        spMassTo.setAdapter(adapter);
    }

    @OnItemSelected(R.id.spMassFrom)
    public void spinnerItemFrom(Spinner spinner, int position){
        this.convertFrom = spinner.getItemAtPosition(position).toString().split(" ")[0];
    }

    @OnItemSelected(R.id.spMassTo)
    public void spinnerItemTo(Spinner spinner, int position){
        this.convertTo = spinner.getItemAtPosition(position).toString().split(" ")[0];
    }

    @OnClick(R.id.btnMassConvert)
    public void convertLength(){
        Double valueToConvert = 0.0;
        try{
            valueToConvert = getValueToConvert(Double.parseDouble(this.etMassValueToConvert.getText().toString()));
            showResult(valueToConvert);
        }catch (Exception e){
            Toast.makeText(this, "Bad input!", Toast.LENGTH_SHORT).show();
        }
    }
    private Double getValueToConvert(double valueToConvert) throws InputException{
        if (valueToConvert <= 0.0){
            throw new InputException();
        }
        return valueToConvert;
    }
    private void showResult(Double valueToConvert) {
        Double result = 0.0;
        Intent intent = new Intent(this, ResultActivity.class);

        intent.putExtra(ResultActivity.KEY_CONVERT_FROM, this.convertFrom);
        intent.putExtra(ResultActivity.KEY_CONVERT_TO, this.convertTo);
        intent.putExtra(ResultActivity.KEY_VALUE, valueToConvert);
        if (this.convertFrom.equals(this.convertTo)){
            result = valueToConvert;
        }
        if (this.convertFrom.equals("kilograms") && this.convertTo.equals("pounds")){
            result = valueToConvert * 2.20462262;
        }
        if (this.convertFrom.equals("pounds") && this.convertTo.equals("kilograms")){
            result = valueToConvert / 2.2046;
            Log.d("TAG", result.toString());
        }
        intent.putExtra(ResultActivity.KEY_CONVERTED_VALUE, result);
        startActivity(intent);
    }
}
